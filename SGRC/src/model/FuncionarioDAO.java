package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import entity.Funcionario;
import util.ConexaoBanco;

public class FuncionarioDAO {
	
	public boolean loginFuncionario(Funcionario funcionario) {
		boolean retorno = false;
		String sql = "SELECT * FROM funcionario WHERE email = ? and senha = ? limit 1;";

		ConexaoBanco conexaoBanco = new ConexaoBanco();

		PreparedStatement executorComandos = conexaoBanco.getExecutorComandos(sql);

		try {
			executorComandos.setString(1, funcionario.getEmail());
			executorComandos.setString(2, funcionario.getSenha());
			
			ResultSet rs = executorComandos.executeQuery();
			if (rs.next()) {
				// Preenche dados do usuario
				//usuario.setDS_Nome(rs.getString("DS_Nome"));

				
				retorno = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conexaoBanco.fecharConexao();
		}

		return retorno;
	}

}

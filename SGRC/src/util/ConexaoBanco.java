package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ConexaoBanco {
	
	private Connection conexao;
	private PreparedStatement executorComandos;

	public PreparedStatement getExecutorComandos(String sql) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conexao = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/SGRC", 
					"root", 
					"qw23er");
			executorComandos = conexao.prepareStatement(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return executorComandos;
	}
	
	public void fecharConexao() {
		try {
			executorComandos.close();
			conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

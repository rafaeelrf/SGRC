package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import control.FuncionarioControle;
import entity.Funcionario;


public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email;
		String senha;
		
		email = request.getParameter("email");
		senha = request.getParameter("senha");
		
		Funcionario funcionario = new Funcionario();
		
		funcionario.setEmail(email);
		funcionario.setSenha(senha);
		
		FuncionarioControle fc = new FuncionarioControle();
		
		boolean sucesso = fc.loginFuncionario(funcionario);
		
		if (sucesso) {
			// Abre uma sess�o para o funcionario
			HttpSession session = request.getSession(true);
			session.setAttribute("idSession", funcionario);
			
			response.sendRedirect("index/principal.jsp");
		} else {
			response.sendRedirect("index/");
		}

	}

}

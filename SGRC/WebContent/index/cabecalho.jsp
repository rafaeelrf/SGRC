<%
	if (session.getAttribute("idSession") == null) {
		response.sendRedirect("../index");
	}
%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>S.G.R.C - Sistema de gest�o empresarial</title>

<link rel="stylesheet" type="text/css" href="../css/grid960/reset.css">
<link rel="stylesheet" type="text/css" href="../css/grid960/grid.css">
<link rel="stylesheet" type="text/css" href="../css/style.css">

</head>
<body>

<div class="container_16">
	<div class="grid_16">

		<div class="barra_topo">
			<div class="logo_topo"><a href="../index/principal.jsp">S.G.R.C</a></div>
			<div class="sair"><a href="../index/logout.jsp?sair=1">Sair</a></div>

		</div>

		<div class="barra_menu">
			<div class="menu">
			
				<li><a href="../index/principal.jsp">Menu principal</a></li>
				
				<li>Gerenciar Funcion�rio
					<ul>
						<li><a href="../gerFunc/cadastrar_funcionario.jsp">Cadastrar funcin�rio</a></li>
						<li><a href="../gerFunc/pesquisar_funcionario.jsp">Pesquisar funcin�rio</a></li>
					</ul>
				</li>
				
				<li>Gerenciar Empresa
					<ul>
						<li><a href="../gerEmp/cadastrar_empresa.jsp">Cadastrar empresa</a></li>
						<li><a href="../gerEmp/pesquisar_empresa.jsp">Pesquisar empresa</a></li>
					</ul>
				</li>
				
				<li>Gerenciar Cliente
					<ul>
						<li><a href="../gerCli/cadastrar_cliente.jsp">Cadastrar cliente</a></li>
						<li><a href="../gerCli/pesquisar_cliente.jsp">Pesquisar cliente</a></li>
					</ul>
				</li>
				
				<li><a href="#">Liberar cr�ditos</a></li>
			
			</div>
			
		</div>
	
	</div> <!-- grid_16 -->

</div> <!-- container -->

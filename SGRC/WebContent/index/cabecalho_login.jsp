<%
	if (session.getAttribute("idSession") != null) {
		response.sendRedirect("principal.jsp");
	}
%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>S.G.R.C - Sistema de gest�o empresarial</title>

<link rel="stylesheet" type="text/css" href="../css/grid960/reset.css">
<link rel="stylesheet" type="text/css" href="../css/grid960/grid.css">
<link rel="stylesheet" type="text/css" href="../css/style.css">

</head>
<body>

<div class="container_16">
	<div class="grid_16">

		<div class="barra_topo">
			<div class="logo_topo"><a href="../index">S.G.R.C</a></div>
		</div>
	
	</div> <!-- grid_16 -->

</div> <!-- container -->